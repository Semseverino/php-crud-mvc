<?php

// alterar modo de exibição do erros do var_dump
ini_set("display_errors", 1);
ini_set("error_reporting", E_ALL);
ini_set('xdebug.overload_var_dump', 1);

ob_start();
session_start();

require './vendor/autoload.php';

use CoffeeCode\Router\Router;

$router = new Router("http://localhost:2020/crudmvc", "@");

$router->namespace("App\Controllers");

$router->group(null);
$router->get("/", "AuthController@loginForm", "web.loginForm");
$router->post("/login", "AuthController@login", "web.login");

$router->get("/home", "WebController@index", "web.index");

$router->get("/create", "WebController@create", "web.create");
$router->post("/store", "WebController@store", "web.store");

$router->get("/edit/{id}", "WebController@edit", "web.edit");
$router->post("/update/{id}", "WebController@update", "web.update");

$router->get("/destroy/{id}", "WebController@destroy", "web.destroy");

$router->get("/logout", "AuthController@logout", "web.logout");

$router->dispatch();

if ($router->error()) {
    var_dump($router->error());
    die;
}

ob_end_flush();