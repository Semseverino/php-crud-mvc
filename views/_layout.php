<!doctype html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Crud MVC</title>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css">
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="<?=$router->route("web.index")?>">CRUD MVC</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
            <li class="nav-item active">
                <a class="nav-link" href="<?=$router->route("web.index")?>">Home <span class="sr-only">(current)</span></a>
            </li>
            <?php if (logged()): ?>
            <li class="nav-item active">
                <a class="nav-link" href="<?=$router->route("web.logout")?>">Sair</a>
            </li>
            <?php endif; ?>
        </ul>
    </div>
</nav>

<div class="container-fluid">
    <div class="row">
        <div class="col">
            <?php if ($errors = flashMessage("errors")): ?>
                <div class="alert alert-<?=$errors["status"]?>">
                    <?php if (is_array($errors["messages"])): ?>
                        <ul>
                            <?php foreach ($errors["messages"] as $error): ?>
                            <li><?=$error?></li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                    <?php if (is_string($errors["messages"])): ?>
                        <?=$errors["messages"]?>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
        </div>
    </div>

    <?= $v->section("content"); ?>
</div>

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"></script>
<?= $v->section("scripts"); ?>

</body>
</html>
