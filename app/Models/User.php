<?php

namespace App\Models;

use CoffeeCode\DataLayer\DataLayer;
use Exception;

class User extends DataLayer
{
    public function __construct()
    {
        parent::__construct("userstest", ["name", "email", "password"]);
    }

    public function save(): bool
    {
        if (!$this->check() || !$this->passwordHash()) {
            return false;
        }
        return parent::save();
    }

    protected function check(): bool
    {
        $email = null;
        if (!$this->id) {
            $email = $this
                ->find("email = :email", "email={$this->email}")
                ->count();
        } else {
            $email = $this
                ->find("email = :email AND id <> :id", "email={$this->email}&id={$this->id}")
                ->count();
        }

        if ($email) {
            $this->fail = new Exception("E-mail e-mail já está em uso");
            return false;
        }
        return true;
    }

    protected function passwordHash(): bool
    {
        if (empty($this->password) || strlen($this->password) < 6) {
            $this->fail = new Exception("Informe uma senha com pelo menos 6 caracteres.");
            return false;
        }

        if (password_get_info($this->password)["algo"]) {
            return true;
        }

        $this->password = password_hash($this->password, PASSWORD_DEFAULT);
        return true;
    }
}