<?php

function setFlashMessage($status = "info", $messages = null)
{
    \App\Support\Session::set("errors", ["status" => $status, "messages" => $messages]);
    return true;
}

function flashMessage($type = "errors")
{
    /**
     * Example
     * Session::set("errors", ["status" => "danger", "messages" => ["test1","test2","test3"]]);
     */
    if (\App\Support\Session::has($type)) {
        $flash = \App\Support\Session::get($type);
        \App\Support\Session::destroy($type);
        return $flash;
    }
    return null;
}

function logged()
{
    if (\App\Support\Session::has("user")) {
        return true;
    }
    return false;
}